variable "credentials" {
  description = "Shared credentials file location"
  default     = "$HOME/.aws/credentials" #The default location is $HOME/.aws/credentials on Linux and macOS
}

variable "region" {
  default = "eu-central-1"
}

variable "environment" {}

variable "instances" {
  default = 2
}

variable "password" {}

variable "owner" {
  description = "Specify Owner tag"
}

variable "dept" {
  description = "Specify Department tag"
}

variable "role" {
  description = "Specify Role tag"
}

variable "ami" {
  default = "ami-03b092fc9a1b88eb0" #avi networks demo webserver
}
